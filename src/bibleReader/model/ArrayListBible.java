package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Jason Gombas (provided the implementation)
 */
public class ArrayListBible implements Bible {

	// The Fields
	private String version;
	private String title;
	private ArrayList<Verse> theVerses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version
	 *            the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.theVerses = new ArrayList<Verse>(verses);
		this.version = verses.getVersion();
		this.title = verses.getDescription();
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	@Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	@Override
	public String getVersion() {
		return this.version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */
	@Override
	public String getTitle() {
		return this.title;
	}

	/**
	 * @param ref
	 *            the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */
	@Override
	public boolean isValid(Reference ref) {
		for (Verse verse : theVerses) {
			if (verse.getReference().equals(ref)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the text of the verse with the given reference
	 * 
	 * @param ref
	 *            the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */
	@Override
	public String getVerseText(Reference r) {
		for (Verse verse : theVerses) {
			if (verse.getReference().equals(r)) {
				return verse.getText();
			}
		}
		return null;
	}

	/**
	 * Return a Verse object for the corresponding Reference.
	 * 
	 * @param ref
	 *            A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	@Override
	public Verse getVerse(Reference r) {
		for (Verse verse : theVerses) {
			if (verse.getReference().equals(r)) {
				return verse;
			}
		}
		return null;
	}

	/**
	 * This retrieves the specified Verse given the book, chapter, and verse number.
	 * 
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse
	 *            the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if the
	 *         reference is invalid.
	 */
	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		for (Verse vers : theVerses) {
			if (vers.getReference().equals(ref)) {
				return vers;
			}
		}
		return null;
	}

	/**
	 * Gets all the verses for this ArrayListBible
	 * 
	 * @return Returns all the verses in a VerseList
	 */
	@Override
	public VerseList getAllVerses() {
		return new VerseList(version, title, theVerses);
	}

	/**
	 * Returns a VerseList of all verses containing <i>phrase</i>, which may be a
	 * word, sentence, or whatever. This method just does simple string matching, so
	 * if <i>phrase</i> is <i>eaten</i>, verses with <i>beaten</i> will be included.
	 * 
	 * @param phrase
	 *            the word/phrase to search for.
	 * @return a VerseList of all verses containing <i>phrase</i>, which may be a
	 *         word, sentence, or whatever. If there are no such verses, returns an
	 *         empty VerseList. In all cases, the version will be set to the version
	 *         of the Bible (via getVersion()) and the description will be set to
	 *         parameter <i>phrase</i>.
	 */
	@Override
	public VerseList getVersesContaining(String phrase) {
		ArrayList<Verse> verseArrayList = new ArrayList<Verse>();
		String lowerPhrase = phrase.toLowerCase();
		if (phrase.equals("")) {
			return new VerseList(version, title, verseArrayList);
		}
		for (Verse verse : theVerses) {
			if (verse.getText().toLowerCase().contains(lowerPhrase)) {
				verseArrayList.add(verse);
			}
		}
		return new VerseList(version, title, verseArrayList);
	}

	/**
	 * Returns a ReferenceList of all references for verses containing
	 * <i>phrase</i>, which may be a word, sentence, or whatever. This method just
	 * does simple string matching, so if <i>phrase</i> is <i>eaten</i>, verses with
	 * <i>beaten</i> will be included.
	 * 
	 * @param phrase
	 *            the phrase to search for
	 * @return a ReferenceList of all references for verses containing
	 *         <i>phrase</i>, which may be a word, sentence, or whatever. If there
	 *         are no such verses, returns an empty ReferenceList.
	 */
	@Override
	public ReferenceList getReferencesContaining(String phrase) {
		ArrayList<Reference> refArrayList = new ArrayList<Reference>();
		String lowerPhrase = phrase.toLowerCase();
		if (phrase.equals("")) {
			return new ReferenceList(refArrayList);
		}
		for (Verse verse : theVerses) {
			if (verse.getText().toLowerCase().contains(lowerPhrase)) {
				refArrayList.add(verse.getReference());
			}
		}
		return new ReferenceList(refArrayList);
	}

	/**
	 * @param references
	 *            a ReferenceList of references for which verses are being requested
	 * @return a VerseList with each element being the Verse with that Reference
	 *         from this Bible, or null if the particular Reference does not occur
	 *         in this Bible. Thus, the size of the returned list will be the same
	 *         as the size of the references parameter, with the items from each
	 *         corresponding. The version will be set to the version of the Bible
	 *         (via getVersion()) and the description will be set "Arbitrary list of
	 *         Verses".
	 */
	@Override
	public VerseList getVerses(ReferenceList references) {
		ArrayList<Verse> verseArrayList = new ArrayList<Verse>();
		for (Reference reference : references) {
			verseArrayList.add(getVerse(reference));
		}
		return new VerseList(version, title, verseArrayList);
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 7
		return -1;
	}

	@Override
	public ReferenceList getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ReferenceList getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ReferenceList getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ReferenceList getReferencesForChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ReferenceList getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ReferenceList getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ReferenceList getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}
}
