package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 * @author Jason Gombas (provided implementation of readATV method)
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. Returns null if the file
	 * isn't formatted correctly or if there was an invalid abbreviation for a book
	 * of the bible. If something went wrong with reading the file, null will be
	 * returned as well. 
	 * 
	 * @param bibleFile
	 *            The file containing a Bible in ATV format.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		ArrayList<Verse> verseArrayList = new ArrayList<Verse>();
		try {
			// Set the default version and description to the empty String
			String version = "";
			String description = "";
			FileReader fRead = new FileReader(bibleFile);
			BufferedReader bRead = new BufferedReader(fRead);
			String nextLine = bRead.readLine();
			String[] arrayFirstLine = nextLine.split(":");
			// Parse the first line of the input file. Determine version and description
			if (arrayFirstLine.length == 2) {
				version = arrayFirstLine[0];
				description = arrayFirstLine[1].substring(1);
			} else if (arrayFirstLine[0].equals("")) {
				version = "unknown";
			} else {
				version = arrayFirstLine[0];
			}
			nextLine = bRead.readLine();
			while (nextLine != null) {
				// Construct Verses from the rest of the lines in the input file and add to an
				// ArrayList
				String[] atSplit = nextLine.split("@");
				BookOfBible book = BookOfBible.getBookOfBible(atSplit[0]);
				if (book == null) {
					bRead.close();
					return null;
				}
				String[] semicolonSplit = atSplit[1].split(":");
				int chapter = Integer.parseInt(semicolonSplit[0]);
				int verse = Integer.parseInt(semicolonSplit[1]);
				String text = atSplit[2];
				verseArrayList.add(new Verse(book, chapter, verse, text));
				nextLine = bRead.readLine();
			}
			bRead.close();
			return new VerseList(version, description, verseArrayList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("File was not found");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("An IO Exception has occured in the BibleIO class.");
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			System.out.println("Index was out of bounds in BibleIO class.");
		}
		return null;
	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile
	 *            The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		// TODO Implement me: Stage 8

		// The XMV is sort of XML-like, but it doesn't have end tags.
		// No description of the file format is given here.
		// You need to look at the file to determine how it should be parsed.

		// TODO Documentation: Stage 8 (Update the Javadoc comment to describe
		// the format of the file.)
		return null;
	}

	// Note: In the following methods, we should really ensure that the file
	// extension is correct
	// (i.e. it should be ".atv"). However for now we won't worry about it.
	// Hopefully the GUI code
	// will be written in such a way that it will require the extension to be
	// correct if we are
	// concerned about it.

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file
	 *            The file that the Bible should be written to.
	 * @param bible
	 *            The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		// TODO Implement me: Stage 8
		// Don't forget to write the first line of the file.
		// HINT: This and the next method are very similar. It seems like you
		// might be
		// able to implement one of them and then call it from the other one.
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file
	 *            The file that the Bible should be written to.
	 * @param description
	 *            The contents that will be placed on the first line of the file,
	 *            formatted appropriately.
	 * @param verses
	 *            The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		// TODO Implement me: Stage 8
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		// TODO Implement me: Stage 8
		// This one should be really simple.
		// My version is 4 lines of code (not counting the try/catch code).
	}
}
